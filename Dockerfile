FROM ubuntu:16.04 
#FROM debian:jessie

LABEL Maintainer Shijie Yao, syao@lbl.gov
# REF : https://github.com/Ecogenomics/CheckM/wiki/Installation

# make alias active
RUN sed -i 's/# alias/alias/' ~/.bashrc

# install the needed programs
ENV PACKAGES wget unzip
RUN apt-get update && apt-get install --yes ${PACKAGES}
#RUN apt-get -y install python-pip

# create download and move into it
WORKDIR /workdir

# download and install picard 
RUN wget https://github.com/broadinstitute/picard/releases/download/2.9.1/picard.jar

# install java runtime (need 1.8)
RUN apt-get install default-jre --yes

# install R 
RUN apt-get update --yes
RUN apt-get install r-base --yes  

# test install : docker run --rm picard java -jar /workdir/picard.jar -h
# use : java jvm-args -jar picard.jar PicardToolName OPTION1=value1 OPTION2=value2... 
