# picard docker image #

Picard is a set of command line tools for manipulating high-throughput sequencing (HTS) data and formats such as SAM/BAM/CRAM and VCF.

The image installed version 2.9.1 (https://github.com/broadinstitute/picard/releases/download/2.9.1/picard.jar, released in May, 2017).
Requires java 1.8 (which is included in the image) to run.

To run:
docker run --rm picard java -jar picard.jar -h